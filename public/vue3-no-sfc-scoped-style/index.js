import { createApp } from "vue"
import App from "./App.js"
import { html, addCSS } from "./utils.js"
import css from "./index.css" assert { type: "css" }
addCSS(css)

createApp({
	components: { App },
	setup() {
		return () => html`<${App} />`
	},
}).mount("#root")
