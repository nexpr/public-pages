import { h } from "vue"
import htm from "htm"

export const html = htm.bind(
	(type, props, ...children) =>
		h(type, props, typeof type === "string" ? children : () => children)
)

export const addCSS = cssss => {
	document.adoptedStyleSheets.push(cssss)
}