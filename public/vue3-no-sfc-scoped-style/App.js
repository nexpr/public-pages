import { html, addCSS } from "./utils.js"
import Component1 from "./Component1.js"
import Component2 from "./Component2.js"
import css from "./App.css" assert { type: "css" }
addCSS(css)

const App = {
	components: {
		Component1,
		Component2,
	},
	setup() {
		return () => html`
			<div class="component-root c-app">
				App:
				<p>ここは赤色</p>
				<${Component1}></>
				<${Component2}></>
			</div>
		`
	},
}

export default App
