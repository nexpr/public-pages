import { html, addCSS } from "./utils.js"
import css from "./Component1.css" assert { type: "css" }
addCSS(css)

const Component1 = {
	props: ["foo"],
	setup(props, { slots }) {
		return () => html`
			<div class="component-root c-component1">
				Component1:
				<p class="blue">ここは青色</p>
				<p>ここは黒色 (App の p タグのスタイルの影響は受けない)</p>
			</div>
		`
	},
}
export default Component1
