import { html, addCSS } from "./utils.js"

const Component2 = {
	props: ["foo"],
	setup(props, { slots }) {
		return () => html`
			<div class="component-root c-component2">
				Component2:
				<p class="blue">
					このコンポーネントにスタイルは指定していないので
					blue クラスだけど黒色
				</p>
			</div>
		`
	},
}
export default Component2
