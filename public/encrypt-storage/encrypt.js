
const encodeText = (text) => new TextEncoder().encode(text)
const decodeText = (ab) => new TextDecoder().decode(ab)

const abToB64 = async (ab) => {
	const u8 = new Uint8Array(ab)
	let str = ""
	for (let i = 0; i < u8.length; i++) {
		str += String.fromCharCode(u8[i])
	}
	return btoa(str)
}

const b64ToAb = async (b64) => {
	const bstr = atob(b64)
	const u8 = new Uint8Array(bstr.length)
	for (let i = 0; i < bstr.length; i++) {
		u8[i] = bstr.charCodeAt(i)
	}
	return u8.buffer
}

const getKey = async (text_key) => {
	const u8 = encodeText(text_key)
	const buf = await crypto.subtle.digest("SHA-256", u8)
	const key = await crypto.subtle.importKey(
		"raw",
		buf,
		"AES-CBC",
		false,
		["encrypt", "decrypt"],
	)
	return key
}

export const encrypt = async (value, text_key) => {
	const data = encodeText(value)
	const key = await getKey(text_key)
	const iv = crypto.getRandomValues(new Uint8Array(16))

	const result = await window.crypto.subtle.encrypt(
		{ name: "AES-CBC", iv },
		key,
		data,
	)
	return [await abToB64(iv), await abToB64(result)].join(",")
}

export const decrypt = async (value, text_key) => {
	const [iv, data] = await Promise.all(value.split(",").map(b64ToAb))
	const key = await getKey(text_key)

	const result = await window.crypto.subtle.decrypt(
		{ name: "AES-CBC", iv },
		key,
		data,
	)
	return decodeText(result)
}
