import { decrypt, encrypt } from "./encrypt.js"

const options = {
	delay: 3000,
	lazy: true,
	cache: true,
	encrypt_key: null,
}

const cache = new Map()
const pending = new Map()
let timer = null

export const setConfig = (opt) => {
	Object.assign(options, opt)
	if (!options.cache) {
		cache.clear()
	}
}

const scheduleWrite = () => {
	clearTimeout(timer)
	timer = setTimeout(() => {
		writeImmediate()
		timer = null
	}, options.delay)
}

export const write = (key, value) => {
	if (!options.encrypt_key) {
		throw new Error("encrypt_key is not specified")
	}
	pending.set(key, value)
	if (options.lazy || !timer) {
		scheduleWrite()
	}
}

export const writeImmediate = async () => {
	if (!options.encrypt_key) {
		throw new Error("encrypt_key is not specified")
	}
	const items = await Promise.all(
		[...pending].map(async ([key, value]) => {
			return { key, value: await encrypt(JSON.stringify(value), options.encrypt_key), raw: value }
		})
	)
	for (const { key, value, raw } of items) {
		localStorage[key] = value
		if (options.cache) {
			cache.set(key, value)
		}
		if (pending.get(key) === raw) {
			pending.delete(key)
		}
	}
}

export const read = async (key) => {
	if (!options.encrypt_key) {
		throw new Error("encrypt_key is not specified")
	}
	if (pending.has(key)) {
		return pending.get(key)
	}
	if (cache.has(key)) {
		return cache.get(key)
	}
	const value = localStorage[key]
	if (!value) return value
	try {
		const raw = JSON.parse(await decrypt(value, options.encrypt_key))
		if (options.cache) {
			cache.set(key, raw)
		}
		return raw
	} catch {
		cache.set(key, undefined)
	}
}

export const remove = (key) => {
	delete localStorage[key]
	cache.delete(key)
	pending.delete(key)
}
