import { html, svg, css, LitElement } from "lit"
import { ref, createRef } from "lit/directives/ref.js"
import { choose } from "lit/directives/choose.js"

const open = svg`
	<svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 -960 960 960" width="20"><path d="M120-120v-320h80v184l504-504H520v-80h320v320h-80v-184L256-200h184v80H120Z"/></svg>
`
const float = svg`
	<svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 -960 960 960" width="20"><path d="m177-120-57-57 184-183H200v-80h240v240h-80v-104L177-120Zm343-400v-240h80v104l183-184 57 57-184 183h104v80H520Z"/></svg>
`
const close = svg`
	<svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 -960 960 960" width="20"><path d="m256-200-56-56 224-224-224-224 56-56 224 224 224-224 56 56-224 224 224 224-56 56-224-224-224 224Z"/></svg>
`

export class FloatableDialog extends LitElement {
	static styles = css`
		.container {
			transition: box-shadow .8s ease 0s;
			position: fixed;
			top: 50%;
			left: 50%;
			right: 50%;
			bottom: 50%;
		}
		.closed {
			transition: box-shadow .3s ease 0s;
		}
		.open {
			box-shadow: 0 0 0 100vmax #0008;
			background: white;
			border-radius: 3px;
			padding: 10px;
			top: 10%;
			left: 10%;
			right: 10%;
			bottom: 10%;

			.header {
				display: flex;
				padding: 4px 4px 8px;;
				border-bottom: 1px solid silver;
				gap: 4px;

				.title {
					margin-right: auto;
				}
			}

			.body {
				padding: 10px;
			}
		}
		.float {
			width: 120px;
			box-shadow: 0 2px 6px 1px #3338;
			background: white;
			border-radius: 3px;
			left: auto;
			top: auto;
			right: 10px;
			bottom: 10px;
			transition: box-shadow .2s ease 0s;

			.header {
				background: #e0e0e0;
				padding: 3px 6px;
				font-size: 12px;
			}

			.body {
				height: 60px;
				display: flex;
				justify-content: center;
				align-items: center;

				& svg {
					width: 70%;
					height: 70%;
				}
			}
		}
		.icon-button {
			padding: 0;
			display: flex;
			justify-content: center;
			align-items: center;
			width: 28px;
			height: 28px;
			flex: none;
			border: 0;
			border-radius: 50%;
			background: transparent;
			padding: 4px;

			&:hover {
				background: #e0e0e0;
			}
		}
	`

	static properties = {
		state: { type: String, reflect: true },
		title: { type: String, reflect: true },
	}

	container_ref = createRef()
	ignore_click = false

	constructor() {
		super()
		this.state = "closed"
	}

	startMove(start_event) {
		const container = this.container_ref.value
		const rect = container.getBoundingClientRect()
		const x = start_event.clientX - rect.x
		const y = start_event.clientY - rect.y
		const handler = (event) => {
			this.ignore_click = true
			container.style.left = (event.clientX - x) + "px"
			container.style.top = (event.clientY - y) + "px"
			container.style.right = "auto"
			container.style.bottom = "auto"
		}
		window.addEventListener("mousemove", handler, { passive: true })
		window.addEventListener("mouseup", () => {
			window.removeEventListener("mousemove", handler)
		}, { once: true })
	}

	open(event) {
		if (event && this.ignore_click) {
			this.ignore_click = false
		} else {
			this.state = "open"
		}
	}

	render() {
		return html`
			<div ${ref(this.container_ref)} class="container ${this.state}">
				${choose(this.state, [
					["open", () => html`
						<div class="header">
							<span class="title">${this.title}</span>
							<button class="icon-button" @click=${() => this.state = "float"}>${float}</button>
							<button class="icon-button" @click=${() => this.state = "closed"}>${close}</button>
						</div>
						<div class="body">
							<slot></slot>
						</div>
					`],
					["float", () => html`
						<div class="header">${this.title}</div>
						<div class="body"
							@mousedown=${this.startMove}
							@click=${this.open}
						>${open}</div>
					`],
				])}
			</div>
		`
	}

	updated(changed) {
		if (this.state !== "float") {
			const container = this.container_ref.value
			container.style.left = ""
			container.style.top = ""
			container.style.right = ""
			container.style.bottom = ""
		}
		if (changed.has("state")) {
			this.dispatchEvent(new Event("state-changed"))
		}
	}
}

if (new URL(import.meta.url).searchParams.has("register")) {
	customElements.define("floatable-dialog", FloatableDialog)
}
